// 载入外挂
var gulp = require('gulp'),
  autoprefixer = require('gulp-autoprefixer');

gulp.task('autopref', function() {
  //找到src目录下css，为其补全浏览器兼容的css
  gulp
    .src('src/**/*.css')
    .pipe(
      autoprefixer({
        browsers: ['last 5 versions', 'Android >= 4.0'],
        cascade: false, //是否美化属性值 默认：true 像这样：
        // remove: true //是否去掉不必要的前缀 默认：true
      })
    )
    //输出到dist文件夹
    .pipe(gulp.dest('dist'));
});
